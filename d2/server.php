<?php

session_start();
//for sending/receiving data

class TaskList {
    //method for adding a new task to the task list
    public function add($description){

        //create a $newTask obj based on the task added by the user. isFinished defaults to false
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false,
            ];
            
        // $_SESSION is another kind of super global variable used by the server to send data in its responses
        //check id $_SESSION exists. If not, create one.
        if ($_SESSION['tasks']===null){
            $_SESSION['tasks'] = array();
        }

        //push the $newTask object to the $_SESSION array
        array_push($_SESSION['tasks'],$newTask);
        
    }

    public function update($id, $description, $isFinished){
        $_SESSION['tasks'][$id]->description = $description;
        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;

    }

    public function remove($id){
        array_splice($_SESSION['tasks'],$id,1);
    }
}

$taskList = new TaskList();
//creates a new TaskList object since classes needs objects

if ($_POST['action']==='add'){
    $taskList->add($_POST['description']);
    //call the add method from taskList and pass the task description to it.
} else if ($_POST ['action'] === 'update'){
    $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}
else if ($_POST ['action'] === 'remove'){
    $taskList->remove($_POST['id']);
}
else if ($_POST ['action'] === 'clear'){
    session_destroy();
}

//redirect the user back to index.php

header('Location: ./index.php');