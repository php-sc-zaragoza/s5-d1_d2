<?php 
    $tasks = ['Git', 'HTML', 'CSS', 'PHP'];

    // _GET is an example of super global variable in PHP. Can send data to and from servers. Will generate parameter on URL unlike _POST
    //isset checks if named sgv exists. If it does, it returns true.

    if (isset($_GET['index'])) {
        $indexGet = $_GET['index'];
        echo "The retrieved task from GET is $tasks[$indexGet]";
    }

    if (isset($_POST['index'])) {
        $indexPost = $_POST['index'];
        echo "The retrieved task from POST is $tasks[$indexPost]";
    }
?>

<!DOCTYPE html>

<html>

    <head>

        <title>S05: Client-Server Communication (GET and POST)</title>

    </head>

    <body>

        <h1>Task index from GET</h1>

        <form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>Task index from POST</h1>

        <form method="POST">

            <select name="index" required>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
            </select>

            <button type="submit">POST</button>

        </form>

    </body>

</html>